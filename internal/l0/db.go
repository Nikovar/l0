package l0

import (
	"database/sql"
	"encoding/json"

	//"log"

	_ "github.com/lib/pq"
)

func Connect(user, password, dbname string) (*sql.DB, error) {
	dbinfo := "user=" + user + " password=" + password + " dbname=" + dbname + " sslmode=disable"
	db, err := sql.Open("postgres", dbinfo)
	if err != nil {
		return nil, err
	}
	//log.Println("Successfully connected")
	db.SetMaxOpenConns(5)
	return db, nil
}

func Insert(db *sql.DB, data []byte) error {
	var order Order
	err := json.Unmarshal(data, &order)
	if err != nil {
		return err
	}

	statement := `
		INSERT INTO users (name, phone, zip, city, address, region, email)
		VALUES ($1, $2, $3, $4, $5, $6, $7) ON CONFLICT (name)
		DO NOTHING`
	_, err = db.Exec(statement, order.Delivery_Name.Name,
		order.Delivery_Name.Phone, order.Delivery_Name.Zip,
		order.Delivery_Name.City, order.Delivery_Name.Address,
		order.Delivery_Name.Region, order.Delivery_Name.Email,
	)
	if err != nil {
		return err
	}

	statement = `
		INSERT INTO payments (transaction, request_id, currency, provider, amount, payment_dt, bank, delivery_cost, goods_total, custom_fee)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) ON CONFLICT (transaction)
		DO NOTHING;`
	_, err = db.Exec(statement, order.Payment_Id.Transaction, order.Payment_Id.Request_Id,
		order.Payment_Id.Currency, order.Payment_Id.Provider,
		order.Payment_Id.Amount, order.Payment_Id.Payment_Dt,
		order.Payment_Id.Bank, order.Payment_Id.Delivery_Cost,
		order.Payment_Id.Goods_Total, order.Payment_Id.Custom_Fee,
	)
	if err != nil {
		return err
	}

	statement = `
		INSERT INTO items (order_uid, chrt_id, track_number, price, rid, name, sale, size, total_price, nm_id, brand, status)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12) ON CONFLICT (chrt_id)
		DO NOTHING;`
	for _, val := range order.Items {
		_, err = db.Exec(statement, order.Order_Uid, val.Chrt_Id,
			val.Track_Number, val.Price, val.Rid, val.Name,
			val.Sale, val.Size, val.Total_Price, val.Nm_Id,
			val.Brand, val.Status)
		if err != nil {
			return err
		}
	}

	statement = `
		INSERT INTO orders (order_uid, track_number, entry, locale, internal_signature, customer_id, delivery_service, shardkey, sm_id, date_created, oof_shard, delivery_name, payment_transaction)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13) ON CONFLICT (order_uid)
		DO NOTHING;`
	_, err = db.Exec(statement, order.Order_Uid, order.Track_Number, order.Entry,
		order.Locale, order.Internal_Signature, order.Customer_Id,
		order.Delivery_Service, order.ShardKey, order.Sm_Id, order.Date_Created, order.Oof_Shard,
		order.Delivery_Name.Name, order.Payment_Id.Transaction,
	)
	if err != nil {
		return err
	}

	return nil
}

func Select(db *sql.DB, id string) ([]byte, error) {
	var result []byte
	var data Order
	var items []Item
	res := db.QueryRow("Select * from orders where order_uid ilike'" + id + "'")
	err := res.Scan(&data.Order_Uid, &data.Track_Number,
		&data.Entry, &data.Locale, &data.Internal_Signature,
		&data.Customer_Id, &data.Delivery_Service, &data.ShardKey,
		&data.Date_Created, &data.Oof_Shard, &data.Delivery_Name.Name,
		&data.Payment_Id.Transaction)
	if err != nil {
		return nil, err
	}

	res = db.QueryRow("Select * from payments where transaction ilike'" + data.Payment_Id.Transaction + "'")
	err = res.Scan(&data.Payment_Id.Transaction, &data.Payment_Id.Request_Id,
		&data.Payment_Id.Currency, &data.Payment_Id.Provider,
		&data.Payment_Id.Amount, &data.Payment_Id.Payment_Dt,
		&data.Payment_Id.Bank, &data.Payment_Id.Delivery_Cost,
		&data.Payment_Id.Delivery_Cost, &data.Payment_Id.Goods_Total,
		&data.Payment_Id.Custom_Fee)
	if err != nil {
		return nil, err
	}

	res = db.QueryRow("Select * from users where name ilike'" + data.Delivery_Name.Name + "'")
	err = res.Scan(&data.Delivery_Name.Name, &data.Delivery_Name.Phone,
		&data.Delivery_Name.Zip, &data.Delivery_Name.City,
		&data.Delivery_Name.Address, &data.Delivery_Name.Region,
		&data.Delivery_Name.Email,
	)
	if err != nil {
		return nil, err
	}

	rows, err := db.Query("Select * from items where transaction ilike'" + data.Order_Uid + "'")
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var item Item
		var order_id string
		err = rows.Scan(order_id, &item.Chrt_Id, &item.Track_Number,
			&item.Price, &item.Rid, &item.Name, &item.Sale,
			&item.Size, &item.Total_Price, &item.Nm_Id,
			&item.Brand, &item.Status)
		if err != nil {
			return nil, err
		}
		items = append(items, item)
	}
	data.Items = items
	result, err = json.Marshal(data)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func SelectForCache(db *sql.DB) (map[string]Order, error) {
	result := make(map[string]Order)
	res, err := db.Query("Select * from orders")
	if err != nil {
		return nil, err
	}
	for res.Next() {
		var data Order
		var items []Item
		err := res.Scan(&data.Order_Uid, &data.Track_Number,
			&data.Entry, &data.Locale, &data.Internal_Signature,
			&data.Customer_Id, &data.Delivery_Service, &data.ShardKey, &data.Sm_Id,
			&data.Date_Created, &data.Oof_Shard, &data.Delivery_Name.Name,
			&data.Payment_Id.Transaction)
		if err != nil {
			return nil, err
		}

		response := db.QueryRow("Select * from payments where transaction ilike'" + data.Payment_Id.Transaction + "'")
		err = response.Scan(&data.Payment_Id.Transaction, &data.Payment_Id.Request_Id,
			&data.Payment_Id.Currency, &data.Payment_Id.Provider,
			&data.Payment_Id.Amount, &data.Payment_Id.Payment_Dt,
			&data.Payment_Id.Bank, &data.Payment_Id.Delivery_Cost, &data.Payment_Id.Goods_Total,
			&data.Payment_Id.Custom_Fee)
		if err != nil {
			return nil, err
		}

		response = db.QueryRow("Select * from users where name ilike'" + data.Delivery_Name.Name + "'")
		err = response.Scan(&data.Delivery_Name.Name, &data.Delivery_Name.Phone,
			&data.Delivery_Name.Zip, &data.Delivery_Name.City,
			&data.Delivery_Name.Address, &data.Delivery_Name.Region,
			&data.Delivery_Name.Email,
		)
		if err != nil {
			return nil, err
		}

		rows, err := db.Query("Select * from items where order_uid ilike'" + data.Order_Uid + "'")
		if err != nil {
			return nil, err
		}
		for rows.Next() {
			var item Item
			var order_id string
			err = rows.Scan(&order_id, &item.Chrt_Id, &item.Track_Number,
				&item.Price, &item.Rid, &item.Name, &item.Sale,
				&item.Size, &item.Total_Price, &item.Nm_Id,
				&item.Brand, &item.Status)
			if err != nil {
				return nil, err
			}
			items = append(items, item)
		}
		data.Items = items
		result[data.Order_Uid] = data
	}

	return result, nil
}
