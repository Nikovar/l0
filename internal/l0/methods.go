package l0

import (
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

func (c *Config) GetConfig(configPath string) error {
	file, err := os.Open(configPath)
	if err != nil {
		return err
	}

	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(file)

	d := yaml.NewDecoder(file)

	if err := d.Decode(&c); err != nil {
		return err
	}
	//defer file.Close()
	return nil
}

func (c *Cache) Set(key string, value Order) {

	c.Lock()

	defer c.Unlock()

	c.Items[key] = value

}

func (c *Cache) Get(key string) Order {

	c.RLock()

	defer c.RUnlock()

	return c.Items[key]

}

func (c *Cache) Init() {
	c.Lock()

	defer c.Unlock()
	c.Items = make(map[string]Order)
}
