package l0

import (
	"sync"
)

type Cache struct {
	sync.RWMutex
	Items map[string]Order
}

type Order struct {
	Order_Uid          string   `json:"order_uid"`
	Track_Number       string   `json:"track_number"`
	Entry              string   `json:"entry"`
	Delivery_Name      Delivery `json:"delivery"`
	Payment_Id         Payment  `json:"payment"`
	Items              []Item   `json:"items"`
	Locale             string   `json:"locale"`
	Internal_Signature string   `json:"internal_signature"`
	Customer_Id        string   `json:"customer_id"`
	Delivery_Service   string   `json:"delivery_service"`
	ShardKey           string   `json:"shardkey"`
	Sm_Id              int      `json:"sm_id"`
	Date_Created       string   `json:"date_created"`
	Oof_Shard          string   `json:"oof_shard"`
}

type Delivery struct {
	Name    string `json:"name"`
	Phone   string `json:"phone"`
	Zip     string `json:"zip"`
	City    string `json:"city"`
	Address string `json:"address"`
	Region  string `json:"region"`
	Email   string `json:"email"`
}

type Payment struct {
	Transaction   string `json:"transaction"`
	Request_Id    string `json:"request_id"`
	Currency      string `json:"currency"`
	Provider      string `json:"provider"`
	Amount        int    `json:"amount"`
	Payment_Dt    int    `json:"payment_dt"`
	Bank          string `json:"bank"`
	Delivery_Cost int    `json:"delivery_cost"`
	Goods_Total   int    `json:"goods_total"`
	Custom_Fee    int    `json:"custom_fee"`
}

type Item struct {
	Chrt_Id      int    `json:"chrt_id"`
	Track_Number string `json:"track_number"`
	Price        int    `json:"price"`
	Rid          string `json:"rid"`
	Name         string `json:"name"`
	Sale         int    `json:"sale"`
	Size         string `json:"size"`
	Total_Price  int    `json:"total_price"`
	Nm_Id        int    `json:"nm_id"`
	Brand        string `json:"brand"`
	Status       int    `json:"status"`
}

type Config struct {
	Local struct {
		Users []struct {
			Login    string `yaml:"login"`
			Password string `yaml:"password"`
		} `yaml:"users"`
		Settings struct {
			Addr string `yaml:"addr"`
		} `yaml:"settings"`
	}
	Nats struct {
		ServerId string `yaml:"clusterid"`
		ClientId string `yaml:"clientid"`
	}
	Psql struct {
		User     string `yaml:"username"`
		Password string `yaml:"password"`
		Database string `yaml:"database"`
	}
}
