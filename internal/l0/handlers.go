package l0

import (
	"github.com/gofiber/fiber/v2"
)

var ServerCache *Cache

func Index(c *fiber.Ctx) error {
	return c.Status(200).SendString("Ping successful " + c.IP())
}
