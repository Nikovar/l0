package l0

import (
	"github.com/gofiber/fiber/v2"
)

func Setup() *fiber.App {
	app := fiber.New(fiber.Config{
		BodyLimit: 64 * 1024,
	})
	app.Get("/", Index)
	return app
}
