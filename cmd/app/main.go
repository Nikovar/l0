package main

import (
	"encoding/json"
	"log"
	"webservice/main/internal/l0"

	"github.com/gofiber/fiber/v2"
	stan "github.com/nats-io/stan.go"
)

const path = "C:\\Users\\nikov\\Projects\\l0\\config\\settings.yml"

func main() {
	var c = l0.Config{}

	c.GetConfig(path)
	var ServerCache = NewCache(&c)

	sc, err := stan.Connect("my_cluster", c.Nats.ClientId, stan.NatsURL("nats://localhost:5222"))
	if err != nil {
		log.Println(err)
	}

	sub, err := sc.Subscribe("l0", func(m *stan.Msg) {
		var data l0.Order
		err = json.Unmarshal(m.Data, &data)
		if err != nil {
			log.Println(err)
		}

		ServerCache.Items[data.Order_Uid] = data
		db_conn, err := l0.Connect(c.Psql.User, c.Psql.Password, c.Psql.Database)
		if err != nil {
			log.Println(err)
		}
		err = l0.Insert(db_conn, m.Data)
		if err != nil {
			log.Print(err)
		}
		defer db_conn.Close()
	})

	if err != nil {
		log.Println(err)
	}

	defer sub.Unsubscribe()
	defer sc.Close()
	app := l0.Setup()
	app.Get("/*", func(c *fiber.Ctx) error {
		id := c.Params("*")
		b, err := json.Marshal(ServerCache.Items[id])
		if err != nil {
			log.Println(err)
			return c.SendStatus(500)
		}
		return c.Status(200).Send(b)
	})
	log.Fatal(app.Listen(":6666"))

}

func NewCache(c *l0.Config) *l0.Cache {

	items := make(map[string]l0.Order)

	cache := l0.Cache{
		Items: items,
	}
	db, err := l0.Connect(c.Psql.User, c.Psql.Password, c.Psql.Database)
	if err != nil {
		log.Print(err)
	}
	if db == nil {
		log.Println("No connection")
		return nil
	}
	data, err := l0.SelectForCache(db)
	if err != nil {
		log.Print(err)
	}
	cache.Items = data
	defer db.Close()

	return &cache
}
