package l0

// import (
// 	"log"

// 	stan "github.com/nats-io/stan.go"
// )

// func NatsSubscribe(c *Config) {
// 	sc, _ := stan.Connect(c.Nats.ServerId, c.Nats.ClientId)

// 	// Simple Synchronous Publisher
// 	sc.Publish("foo", []byte("Hello World")) // does not return until an ack has been received from NATS Streaming

// 	// Simple Async Subscriber
// 	sub, _ := sc.Subscribe("foo", func(m *stan.Msg) {
// 		log.Printf("Received a message: %s\n", string(m.Data))
// 	})
// 	sub.Unsubscribe()
// 	defer sc.Close()
// }
